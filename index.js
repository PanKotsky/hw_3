import {NewpostRepository} from "./src/repositories/Newpost";

// додаємо новий запис, та повертаємо його з новим id
const data = {
    title: 'У зоопарку Чернігова лисичка народила лисеня',
    text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
}

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc['func'] = paramValue[0].replace('--', '');
        acc['param'] = paramValue[1];
    }
    return acc;
}, {});

const func = options.func;
const id = options.param;
let result;

switch (func) {
    case 'newsposts':
        result = NewpostRepository.findAll()
        break;
    case 'newspost':
        result = NewpostRepository.findById(id);
        break;
    case 'createdNewspost':
        result = NewpostRepository.create(data);
        break;
    case 'updatedNewsposts':
        result = NewpostRepository.updateById(id, {title: "Маленька лисичка"});
        break;
    case 'deletedById':
        result = NewpostRepository.deleteById(id);
        break;
    default:
        throw new Error('The action doesn\'t allowed');
}


console.log(result);