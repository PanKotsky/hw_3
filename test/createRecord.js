const NewpostRepository = require("../src/repositories/Newpost");

// додаємо новий запис, та повертаємо його з новим id
const data = {
    title: 'У зоопарку Чернігова лисичка народила лисеня',
    text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
}

NewpostRepository.create(data).then(result => console.log(result));
