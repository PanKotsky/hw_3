// @ts-ignore
import {Schema} from "inspector";
const fs = require('fs');
import * as path from "path";

type TableSchema = {};

class Newpost {
    // @ts-ignore
    id: Number;
    // @ts-ignore
    title: String;
    // @ts-ignore
    text: String;
    // @ts-ignore
    createDate: Date;
}

const AllSchemas: object = {
    'Newpost': new Newpost
};

class FileDB
{
    schemaTable: TableSchema;
    table: string;

    constructor(table: string) {
        this.table = table;
        // @ts-ignore
        this.schemaTable = AllSchemas[table];
    }

    prepareDataRead(data: string): object[] {
        return data ? JSON.parse(data) : [];
    }

    prepareDataWrite(data: Array<object>, newRecord: object) {
        data.push(newRecord);

        if (typeof data === 'object') {
            return JSON.stringify(data);
        } else {
            console.error(data);
            throw new Error('Unexpected format of data');
        }
    }

    checkFile(filename: string): boolean {
        return fs.existsSync(filename);
    }

    async read(): Promise<Array<object>> {
        let filename = this.getFilename();
        // створити файл за відсутності
        !this.checkFile(filename) && fs.writeFileSync(filename, '');
        return this.prepareDataRead(await fs.promises.readFile(filename, 'utf8'));
    }

    write(newRecord: object): void {
        let filename = this.getFilename();
        !this.checkFile(filename) && fs.writeFileSync(filename, '');
        this.read()
            .then(records => {
                fs.promises.writeFile(filename, this.prepareDataWrite(records, newRecord));
            })
            .catch(err => console.error(err));
    }

    async updateById(id: string|number, record: object): Promise<object|null|void> {
        return this.read()
            .then(records => {
                let result = null;
                // @ts-ignore
                let index = records.findIndex(value => value.id == id);

                if (index !== -1) {
                    for (const [field, value] of Object.entries(record)) {
                        // @ts-ignore
                        records[index][field] = value;
                    }

                    result = records[index];
                    fs.promises.writeFile(this.getFilename(), JSON.stringify(records));
                }

                return result;
            })
            .catch(err => console.error(err.message));
    }

    async deleteById(id: string|number): Promise<string|number|null|void> {
        let filename = this.getFilename();

        if (!this.checkFile(filename)) {
            throw new Error('File DB doesn`t exist');
        }

        return this.read()
            .then(records => {
                let result = null;
                // @ts-ignore
                let index = records.findIndex(value => value.id === Number(id));
                if (index !== -1) {
                    records.splice(index, 1);
                    fs.promises.writeFile(this.getFilename(), JSON.stringify(records));
                    result = id;
                }

                return result;
            })
            .catch(err => console.error(err.message));
    }

    private getFilename() {
        return path.join(__dirname, '../db/' + this.table + '.txt');
    }
}

export = FileDB;