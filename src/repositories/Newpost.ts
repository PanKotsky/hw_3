import { BaseRepository } from "./BaseRepository";
import { Newpost } from "../entities/Newpost"

class NewpostRepository extends BaseRepository<Newpost> {

    private entity = Newpost;

    constructor() {
        super('Newpost');
    }

    getEntity(): object {
        return new this.entity;
    }
}

export = new NewpostRepository;