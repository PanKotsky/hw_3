export interface IRead<T> {
    findAll(): Promise<Array<object>>;
    findById(id: string|number): Promise<object|null|undefined>;
}
