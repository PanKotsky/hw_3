export interface IWrite<T> {
    create(item: object): Promise<string|number|void>;
    updateById(id: string, item: object): object|null;
    deleteById(id: string): Promise<string|number|null|void>;
}
