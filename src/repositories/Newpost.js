"use strict";
const BaseRepository_1 = require("./BaseRepository");
const Newpost_1 = require("../entities/Newpost");
class NewpostRepository extends BaseRepository_1.BaseRepository {
    constructor() {
        super('Newpost');
        this.entity = Newpost_1.Newpost;
    }
    getEntity() {
        return new this.entity;
    }
}
module.exports = new NewpostRepository;
