import {IWrite} from './interfaces/IWrite';
import {IRead} from './interfaces/IRead';
import FileDB from "../middleware/FileDB";
import Hydrator from "../middleware/Hydrator";

export abstract class BaseRepository<T> implements IWrite<T>, IRead<T> {

    protected readonly _collection: FileDB;
    private readonly _hydrator: Hydrator;

    protected constructor(schemaName: string) {
        this._collection = new FileDB(schemaName);
        this._hydrator = new Hydrator;
    }

    private getLastRecord(data: Array<object>): object|null {
        return data.length > 0 ? data[data.length - 1] : null;
    }

    async create(item: object): Promise<string|number|void> {
        return this._collection.read()
            .then(dataExist => {
                let lastRecord = this.getLastRecord(dataExist);
                // 'id' - в ідеалі тут має бути визначений PK, але це ще більше заморочитись
                let newRecordId: number = lastRecord ? ++lastRecord['id' as keyof object] : 1;
                item = this._hydrator.hydrateRecord(this._collection.schemaTable, item, newRecordId);
                this._collection.write(item);

                return newRecordId;
            })
            .catch(err => console.error(err.message));
    }

    async updateById(id: string|number, item: object): Promise<object|null|void> {
        return this._collection.updateById(id, item);
    }

    async deleteById(id: string|number): Promise<string|number|null|void> {
        return this._collection.deleteById(id);
    }
    async findAll(): Promise<Array<object>> {
        return await this._collection.read();
    }
    async findById(id: string|number): Promise<object|null|undefined> {
        return this._collection.read()
            .then(records => {
                // @ts-ignore
                return records ? records.find(value =>  value.id === id) : null;
            });
    }
}